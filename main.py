# coding: utf-8

from bottle import Bottle, run, route, template, request
import datetime

app = Bottle()

@app.route('/')
def top():
    username = 'test sample user name'
    return template('sample' , username=username)
    #return "Hello, world!"

@app.route('/search' , method=["POST"])
def search():
    nowtime = datetime.datetime.now()
    Fstation = request.forms.Fstation
    Lstation = request.forms.Lstation
    comment  = request.forms.comment

    return template('search' 
        , nowtime=nowtime
        , Fstation=Fstation
        , Lstation=Lstation
        , comment=comment
        )

if __name__ == '__main__':
    app.run(host='localhost', port=8080,reloader=True,debug=True)
